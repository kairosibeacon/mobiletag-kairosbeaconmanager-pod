# Documentation technique KairosFire Beacon Manager

![Kairosfire](https://bytebucket.org/kairosibeacon/kairosbeaconmanager-pod/raw/master/doc/kairosfire-rgb.png)


## Guide d'intégration du manager sous Xcode / iOS

### Etape 1: Installation du manager

#### Installation via cocoapods

Nous conseillons fortement une installation via cocoapods, si celui-ci n'est pas déjà installé, lancez cette commande dans le terminal : ```sudo gem install cocoapods```.

Créez ou complétez ensuite le fichier Podfile à la racine de votre projet Xcode :

Cocoapods v1.0 et plus :

```
source 'https://github.com/CocoaPods/Specs.git'
source 'https://bitbucket.org/kairosibeacon/mobiletag-kairosbeaconmanager-pod.git'

platform :ios, '8.0'
target your_target 
	pod 'KairosBeaconManager', '~> 1.5.0'
    pod 'AFNetworking', '~> 3.0'
```

Pour les versions de Cocoapods < 1.0 :

```
source 'https://github.com/CocoaPods/Specs.git'
source 'https://bitbucket.org/kairosibeacon/mobiletag-kairosbeaconmanager-pod.git'

platform :ios, '8.0'
pod 'KairosBeaconManager', '~> 1.5.0'
pod 'AFNetworking', '~> 3.0'
```

Exécutez ensuite la commande ```pod install```.

> **⚠️ Attention :** 
> À partir de cette étape, il faudra dorénavant utiliser le fichier ```.xcworkspace``` pour ouvrir votre projet Xcode à la place de ```.xcproj```

#### Installation manuelle du manager

##### Intégration de la librairie 

Vous devez copier les fichiers ```libKairosBeaconManager.a```, ```KExtrasResources.bundle``` et ```KairosBeaconManager.h``` dans le projet courant. Le plus simple est de glisser-déposer les fichiers dans l'interface de Xcode.
Ces fichiers sont téléchargeable à cette [adresse](https://bitbucket.org/kairosibeacon/kairosbeaconmanager-pod/downloads).


##### Ajout des dépendances

Il est nécessaire d'installer manuellement dans votre projet les dépendances suivantes :

AFNetworking v2.6.3 : [Lien sur github](https://github.com/AFNetworking/AFNetworking/archive/2.6.3.zip)

### Etape 2 : Edition du fichier Plist

L'édition du fichier Plist permet de personnaliser le message de l'écran de demande d'autorisation d'accès aux données de localisation en tâche de fond de l'application.

> ⚠️ Cette modification est **nécessaire** pour le bon fonctionnement du SDK Kairos.

Il est possible de laisser une chaîne de caractères vide, cependant, **nous incitons fortement l'application mobile à utiliser cet espace pour expliquer à l'utilisateur la valeur ajoutée de l'autorisation**.

Dans le fichier PList du projet, rajoutez la clef ```NSLocationAlwaysUsageDescription``` avec en valeur un texte qui invitera l'utilisateur à autoriser l'application à mobile à accéder aux données de localisation en tâche de fond.

Voici un exemple de fenêtre popup qui s'affichera, "Votre Application" sera remplacé par le nom de votre application et le texte intégré dans votre fichier PList sera affiché à la suite de la popup :

![popup](https://bytebucket.org/kairosibeacon/kairosbeaconmanager-pod/raw/6ecdec408eb51ff061a3d41c5c322d07f460ea91/doc/notification3.png)

### Etape 3 : Modification du fichier AppDelegate.m
Pour initialiser le manager, il est nécessaire d'ajouter les lignes suivantes dans la méthode  ```didFinishLaunchingWithOptions```:
> **⚠️ Attention :** 
> Ne pas oublier de remplacer ```app_secret``` et ```app_token``` par les valeurs fournies.


```objectivec
#import "AppDelegate.h"
#import "KairosBeaconManager.h"

/** ⚠️ Mandatory : set protocol for delegate **/
@interface AppDelegate()<KairosBeaconManagerDelegate>

@property (strong, nonatomic) KairosBeaconManager *manager;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication*)application 
didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
     /** ⚠️ Mandatory : initialize beacon manager **/
    self.manager = [KairosBeaconManager sharedInstance:@"app_secret" 
                                         appToken:@"app_token" 
                                         delegate:self];
    
    // optionnal
    [self.manager setDebug:YES];
    
    // ⚠️ mandatory start monitoring for kairos beacons
    [self.manager startBeaconMonitoring];
    
    return YES;
}

/** ⚠️ Mandatory : enable local notification **/
- (void)application:(UIApplication*)application 
            didReceiveLocalNotification:(UILocalNotification*)notification{
    
    if (![self.manager isKNotificationReceived:notification]){
        //write non-kairos notification
    }
    
}

```

```swift

/** ⚠️ Mandatory : set protocol for delegate **/
class AppDelegate: UIResponder, UIApplicationDelegate, KairosBeaconManagerDelegate {
    var manager:KairosBeaconManager?
    
    /** ⚠️ Mandatory : conforms to KairosBeaconManagerDelegate 
     * Will be removed in v1.6
     */
    func onError(_ message: String?) {
        // print(message)
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        /** ⚠️ Mandatory : initialize beacon manager **/
        manager = KairosBeaconManager.sharedInstance("app_secret", 
            appToken: "app_token", 
            delegate: self
        )
        
        // optionnal
        manager?.setDebug(true)

        // ⚠️ mandatory start monitoring for kairos beacons
        manager?.startBeaconMonitoring()
        return true
    }

    /** ⚠️ Mandatory : enable local notification **/
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        if (manager?.isKNotificationReceived(notification))! == false {
            //write non-kairos notification    
        }
    }
}


```


### Etape 4 (optionnelle) : Recherche active de balises iBeacon

Par défaut, le SDK écoute de manière passive les iBeacons à proximité (mode monitoring actif tout le temps, y compris lorsque l'application est en background). 
En utilisant la méthode ```startRanging``` on peut demander au manager d'effectuer une recherche plus aggressive des iBeacons à proximité, cette méthode ne fonctionnera que lorsque l'application est active.  
Pour activer ce mode, on peut ajouter la ligne suivante dans la méthode ```applicationDidBecomeActive``` :

```objectivec
- (void)applicationDidBecomeActive:(UIApplication *)application {
    [self.manager startRanging];
}

```

```swift
func applicationDidBecomeActive(_ application: UIApplication) {
    manager?.startRanging()
}

```

```startRanging``` va activer le ranging de balises iBeacon pendant la durée ```scanDelay``` (voir Annexe > API).


### Etape 5 (Optionnelle) : Paramétrage de la réception des notifications

Il est possible de paramétrer le SDK pour désactiver ou non l'affichage des notifications push. Nous utilisons pour cela les valeurs stockées dans ```NSUSerDefault```. Cela permet au développeur d'applications d'intégrer facilement dans sa page de paramètres un switch pour activer ou non les notifications.

Par défaut, l'affichage des notifications est actif.
Pour activer / désactiver l'affichage des notifications, vous pouvez utiliser le code suivant :

```objectivec
// Importer KairosBeaconManager.h permet d'avoir accès à la constante "kAllowKNotification" 
// qui contient la valeur de la clef utilisée dans NSUserDefault
#import "KairosBeaconManager.h"

[[NSUserDefaults standardUserDefaults] setBool:YES forKey:kAllowKNotification];

```

## Changements (v1.5.0)

* Ajout du bridge swift
* Mise à jour pour AFNetworking v3.1.0
* Améliorations mineures
* Résolution de bugs mineurs 

## Changements (v1.4.6)

* Supression de la dépendance sur GoogleAnalytics
* Amélioration du comportement du SDK en environnement multi SDK
* Compilation de la librairie pour ios 7.0 minimum
* Amélioration de la remontée de statistiques
* Résolution de bugs majeurs 
* Amlioration des tests unitaires et du code

## Annexe

### API :

Les méthodes publiques sont disponible dans l'interface [KairosBeaconManager.h](https://bitbucket.org/kairosibeacon/kairosbeaconmanager-pod/src/master/src/KairosBeaconManager.h?at=master&fileviewer=file-view-default).

## Contacts :

Technique : [Alexandre  Assouad](mailto:assouad@kairosfire.com)

Responsable partenariats : [Pierre-Louis Picot](mailto:picot@kairosfire.com)


